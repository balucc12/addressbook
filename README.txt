# AddressBook #

This app is completely developed using TDD practice.

Repository contains two projects:
1. AddressBook Xcode project
2. Sinatra server used by AddressBook

To run Sinatra server execute
$ ruby server.rb -o 0.0.0.0

You may need to
$ gem install sinatra
if you don't have sinatra installed.

## Running an Xcode project ##

In Constants.m there is a line
NSString* const kCCBaseUrl = @"http://localhost:4567/api";

By default sinatra listens on 4567 port. If you're running sinatra server on remote machine, update localhost address accordingly. You'll also have to do this if you're running your app on device.

########