require 'sinatra'
require 'json'

configure do
	set :logging, true
	set :dump_errors, true
	set :public_folder, Proc.new { File.expand_path( File.join(root, 'Fixtures')) }
end

get '/api/contacts' do
	status 200
	content_type :json
    File.read('Fixtures/_contacts.json')
end

get '/api/contact/:contactid' do
	status 200
	content_type :json
	File.read("Fixtures/#{params[:contactid]}.json")
end
