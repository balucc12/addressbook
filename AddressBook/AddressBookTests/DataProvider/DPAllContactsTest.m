//
//  DPAllContactsTest.m
//  AddressBook
//
//  Created by Marko Cicak on 4/7/15.
//  Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import "DPAllContacts.h"
#import "ContactDao.h"

@interface DPAllContactsTest : XCTestCase
{
    DPAllContacts* provider;
}
@end

@implementation DPAllContactsTest

- (void) setUp
{
    [super setUp];
    provider = [[DPAllContacts alloc] init];
}

- (void) tearDown
{
    [super tearDown];
}

- (void) testSuccessBlock
{
    SREG.coredata = nil;
    ContactDao* contactDao = [[ContactDao alloc] init];
    XCTAssertEqual(contactDao.findAll.count, 0);

    NSArray* contacts = self.mockContacts;
    provider.successBlock(nil, contacts);

    XCTAssertEqual(contactDao.findAll.count, 5);
}

- (NSArray*) mockContacts
{
    NSDictionary* c1 = @{
            @"id" : @"1",
            @"firstname" : @"Igor",
            @"lastname" : @"Stojanović"
    };
    NSDictionary* c2 = @{
            @"id" : @"2",
            @"firstname" : @"Erwin",
            @"lastname" : @"Schrödingher"
    };
    NSDictionary* c3 = @{
            @"id" : @"3",
            @"firstname" : @"Нада",
            @"lastname" : @"Домазет-Гвозден"
    };
    NSDictionary* c4 = @{
            @"id" : @"4",
            @"firstname" : @"Leonardo",
            @"lastname" : @"da Vinci"
    };
    NSDictionary* c5 = @{
            @"id" : @"5",
            @"firstname" : @"Ludwig",
            @"lastname" : @"van Halen"
    };

    return @[ c1, c2, c3, c4, c5 ];
}

@end
