//
// Created by Marko Cicak on 10/27/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import "DataProvider.h"
#import "DataProviderMock.h"

@implementation DataProviderMock

- (void) retrieveAllContacts
{
    self.retrieveAllContactsCalled = YES;
}

- (void) retrieveContactWithID:(NSString*)contactID
{
    self.retrieveContactWithIDCalled = YES;
}

@end
