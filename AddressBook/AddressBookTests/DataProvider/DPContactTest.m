//
//  DPContactTest.m
//  AddressBook
//
//  Created by Marko Cicak on 4/27/15.
//  Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import "DPContact.h"
#import "Contact.h"
#import "ContactDao.h"
#import "Email.h"
#import "NLBaseTests.h"

@interface DPContactTest : NLBaseTests
{
    DPContact* provider;
}
@end

@implementation DPContactTest

- (void) setUp
{
    [super setUp];
    provider = [[DPContact alloc] initWithContactID:@"1"];
    SREG.coredata = nil;
    [self prepareData];
}

- (void) tearDown
{
    provider = nil;
    [super tearDown];
}

- (void) testPath
{
    XCTAssertEqualObjects(provider.path, @"contact/1");
}

- (void) testSuccessBlock
{
    ContactDao* contactDao = [[ContactDao alloc] init];
    NSArray* contacts = contactDao.findAll;
    XCTAssertEqual(contacts.count, 1);
    Contact* c = contacts.firstObject;
    XCTAssertEqualObjects(c.contactID, @"1");
    XCTAssertNil(c.phone);

    NSDictionary* contact = [self mockContact];
    provider.successBlock(nil, contact);

    contacts = contactDao.findAll;
    c = contacts.firstObject;

    XCTAssertEqualObjects(c.contactID, @"1");
    XCTAssertEqualObjects(c.firstName, @"George");
    XCTAssertEqualObjects(c.lastName, @"Harrison");
    XCTAssertEqualObjects(c.phone, @"+381601234567");
    XCTAssertEqualObjects(c.street, @"Danila Kiša 17/A");
    XCTAssertEqualObjects(c.zip, @"21000");
    XCTAssertEqualObjects(c.state, @"Serbia");
    XCTAssertEqual(c.emails.count, 2);
    NSArray* emails = [c.emails.allObjects
            sortedArrayUsingDescriptors:@[ [NSSortDescriptor sortDescriptorWithKey:@"address" ascending:YES] ]];
    Email* e1 = emails[0];
    XCTAssertEqualObjects(e1.address, @"george.harrison@gmail.com");
    Email* e2 = emails[1];
    XCTAssertEqualObjects(e2.address, @"gharrison@yahoo.com");
}

#pragma mark - Private

- (NSDictionary*) mockContact
{
    return @{
            @"contactID" : @"1",
            @"firstname" : @"George",
            @"lastname" : @"Harrison",
            @"phone" : @"+381601234567",
            @"street" : @"Danila Kiša 17/A",
            @"zip" : @"21000",
            @"city" : @"Novi Sad",
            @"state" : @"Serbia",
            @"emails" : @[
                    @"george.harrison@gmail.com",
                    @"gharrison@yahoo.com"
            ]
    };
}

- (void) prepareData
{
    ContactDao* contactDao = [[ContactDao alloc] init];
    Contact* c1 = [contactDao insertNew];
    c1.contactID = @"1";
    c1.firstName = @"George";
    c1.lastName = @"Harrison";
}

@end
