//
// Created by Marko Cicak on 10/27/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface DataProviderMock : NSObject <DataProvider>

@property(nonatomic, assign) BOOL retrieveAllContactsCalled;
@property(nonatomic, assign) BOOL retrieveContactWithIDCalled;

@end
