//
//  FavoriteViewControllerTest.m
//  AddressBook
//
//  Created by Marko Cicak on 4/24/15.
//  Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import "FavoriteViewController.h"
#import "FavoriteViewModel.h"
#import "FavoriteViewModelMock.h"
#import "ContactsViewController.h"
#import "ContactViewController.h"

@interface FavoriteViewController ()
@property(nonatomic, strong) id <FavoriteViewModel> model;

- (void) didTapAddButton:(id)sender;
@end

@interface FavoriteViewControllerTest : XCTestCase
{
    FavoriteViewController* controller;
}
@end

@implementation FavoriteViewControllerTest

- (void) setUp
{
    [super setUp];
    UIStoryboard* s = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    controller = [s instantiateViewControllerWithIdentifier:@"FavoritesSID"];
    controller.model = [[FavoriteViewModelMock alloc] init];
    [controller loadView];
}

- (void) tearDown
{
    controller = nil;
    [super tearDown];
}

- (void) testSelectingContactWithoutPhoneShowsAlertMessage
{
    [UIApplication sharedApplication].delegate.window.rootViewController = controller;
    XCTAssertNil(controller.presentedViewController);
    [controller tableView:nil didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    XCTAssertEqualObjects(controller.presentedViewController.class, UIAlertController.class);
    UIAlertController* alert = (UIAlertController*) controller.presentedViewController;
    XCTAssertEqualObjects(alert.message, @"Contact doesn't have phone number.");
}

- (void) testTappingAccessoryDetailCellIconPushesContactDetails
{
    UINavigationController* nc = [[UINavigationController alloc] initWithRootViewController:controller];
    XCTAssertEqual(nc.viewControllers.count, 1);
    NSIndexPath* path = [NSIndexPath indexPathForRow:0 inSection:0];
    [controller tableView:nil accessoryButtonTappedForRowWithIndexPath:path];
    XCTAssertEqual(nc.viewControllers.count, 2);
    XCTAssertEqualObjects(nc.topViewController.class, ContactViewController.class);
}

- (void) testTappingAddButtonOpensContactsViewController
{
    // controller needs to belong to root hierarchy in order to test modal presentations
    [UIApplication sharedApplication].delegate.window.rootViewController = controller;

    XCTAssertNil(controller.presentedViewController);
    [controller didTapAddButton:nil];
    XCTAssertEqualObjects(controller.presentedViewController.class, UINavigationController.class);
    UINavigationController* nc = (UINavigationController*) controller.presentedViewController;
    XCTAssertEqualObjects(nc.topViewController.class, ContactsViewController.class);
}

@end
