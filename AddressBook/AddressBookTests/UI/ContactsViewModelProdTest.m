//
//  ContactsViewModelProdTest.m
//  AddressBook
//
//  Created by Marko Cicak on 4/10/15.
//  Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import "ContactsViewModelProd.h"
#import "Contact.h"
#import "ContactDao.h"

@interface ContactsViewModelProdTest : XCTestCase
{
    ContactsViewModelProd* model;
}
@end


@implementation ContactsViewModelProdTest

- (void) setUp
{
    [super setUp];
    SREG.coredata = nil;
    [self prepareData];
    model = [[ContactsViewModelProd alloc] init];
}

- (void) tearDown
{
    model = nil;
    [super tearDown];
}

- (void) testSectionsCountIsCorrect
{
    XCTAssertEqual([model numberOfSectionsInTableView:nil], 4);
}

- (void) testNumberOfSectionRowsIsCorrect
{
    XCTAssertEqual([model tableView:nil numberOfRowsInSection:1], 1);
    XCTAssertEqual([model tableView:nil numberOfRowsInSection:2], 2);
}

- (void) testCellHasProperContent
{
    UIStoryboard* s = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UITableViewController* viewController = [s instantiateViewControllerWithIdentifier:@"AllContactsSID"];
    UITableView* tv = viewController.tableView;

    UITableViewCell* cell = [model tableView:tv cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    XCTAssertEqualObjects(cell.textLabel.text, @"John Lennon");
    cell = [model tableView:tv cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:2]];
    XCTAssertEqualObjects(cell.textLabel.text, @"Paul McCartney");
}

- (void) testHeaderTitle
{
    XCTAssertEqualObjects([model tableView:nil titleForHeaderInSection:0], @"H");
    XCTAssertEqualObjects([model tableView:nil titleForHeaderInSection:1], @"L");
    XCTAssertEqualObjects([model tableView:nil titleForHeaderInSection:2], @"M");
    XCTAssertEqualObjects([model tableView:nil titleForHeaderInSection:3], @"S");
}

- (void) testIndexTitles
{
    NSArray* expected = @[ @"H", @"L", @"M", @"S" ];
    NSArray* index = [model sectionIndexTitlesForTableView:nil];
    XCTAssertEqualObjects(expected, index);
}

- (void) prepareData
{
    ContactDao* contactDao = [[ContactDao alloc] init];
    Contact* c1 = [contactDao insertNew];
    c1.contactID = @"1";
    c1.firstName = @"George";
    c1.lastName = @"Harrison";

    Contact* c2 = [contactDao insertNew];
    c2.contactID = @"2";
    c2.firstName = @"John";
    c2.lastName = @"Lennon";

    Contact* c3 = [contactDao insertNew];
    c3.contactID = @"3";
    c3.firstName = @"George";
    c3.lastName = @"Martin";

    Contact* c4 = [contactDao insertNew];
    c4.contactID = @"4";
    c4.firstName = @"Paul";
    c4.lastName = @"McCartney";

    Contact* c5 = [contactDao insertNew];
    c5.contactID = @"5";
    c5.firstName = @"Ringo";
    c5.lastName = @"Starr";

    [contactDao save];
}

@end
