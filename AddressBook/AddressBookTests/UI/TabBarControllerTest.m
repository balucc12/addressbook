//
//  TabBarControllerTest.m
//  AddressBook
//
//  Created by Marko Cicak on 4/2/15.
//  Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import "TabBarController.h"
#import "ContactsViewController.h"
#import "FavoriteViewController.h"

@interface TabBarControllerTest : XCTestCase
{
    TabBarController* viewController;
}
@end


@implementation TabBarControllerTest

- (void) setUp
{
    [super setUp];
    UIStoryboard* s = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    viewController = [s instantiateInitialViewController];
    [viewController loadView];
}

- (void) tearDown
{
    viewController = nil;
    [super tearDown];
}

- (void) testViewControllerHierarchy
{
    XCTAssertEqual(viewController.viewControllers.count, 2);
    XCTAssertEqualObjects([viewController.viewControllers[0] class], UINavigationController.class);
    XCTAssertEqualObjects([viewController.viewControllers[1] class], UINavigationController.class);

    NSArray* controllers = [viewController.viewControllers valueForKey:@"topViewController"];
    XCTAssertEqualObjects([controllers[0] class], FavoriteViewController.class);
    XCTAssertEqualObjects([controllers[1] class], ContactsViewController.class);
}

- (void) testContactsTabIsSelectedInitially
{
    [viewController viewDidLoad];
    [viewController viewWillAppear:NO];
    UINavigationController* nc = (UINavigationController*) viewController.selectedViewController;
    XCTAssertEqualObjects(nc.topViewController.class, ContactsViewController.class);
}

@end
