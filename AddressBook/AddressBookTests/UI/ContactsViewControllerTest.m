//
//  ContactsViewControllerTest.m
//  AddressBook
//
//  Created by Marko Cicak on 4/7/15.
//  Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import "ContactsViewController.h"
#import "DataProvider.h"
#import "DataProviderMock.h"
#import "ContactsViewModel.h"
#import "ContactsViewModelMock.h"
#import "ContactsViewSearchModel.h"
#import "ContactViewController.h"

@interface ContactsViewController ()
@property(nonatomic, strong) id <ContactsViewModel> model;
@property(nonatomic, strong) ContactsViewSearchModel* searchModel;
@end


@interface ContactsViewControllerTest : XCTestCase
{
    ContactsViewController* viewController;
    DataProviderMock* dataProvider;
}
@end


@implementation ContactsViewControllerTest

- (void) setUp
{
    [super setUp];
    dataProvider = DataProviderMock.new;
    SREG.dataProvider = dataProvider;
    UIStoryboard* s = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    viewController = [s instantiateViewControllerWithIdentifier:@"AllContactsSID"];
    [viewController loadView];
    viewController.model = [[ContactsViewModelMock alloc] init];
}

- (void) tearDown
{
    viewController = nil;
    [super tearDown];
}

- (void) testRelationshipsAfterViewDidLoad
{
    ContactsViewController* vc = viewController;
    XCTAssertNotEqualObjects(vc.tableView.dataSource, vc.model);
    XCTAssertNotEqualObjects(vc.searchDisplayController.searchResultsDataSource, vc.searchModel);
    XCTAssertFalse(((ContactsViewModelMock*) vc.model).hasFRCDelegate);
    [vc viewDidLoad];
    XCTAssertEqualObjects(vc.tableView.dataSource, vc.model);
    XCTAssertEqualObjects(vc.searchDisplayController.searchResultsDataSource, vc.searchModel);
    XCTAssertTrue(((ContactsViewModelMock*) vc.model).hasFRCDelegate);
}

- (void) testAllContactsAreRetrievedUponViewAppeared
{
    XCTAssertFalse(dataProvider.retrieveAllContactsCalled);
    [viewController viewWillAppear:NO];
    XCTAssertTrue(dataProvider.retrieveAllContactsCalled);
}

- (void) testSelectingContactCellPushesContactDetailsView
{
    UINavigationController* nc = [[UINavigationController alloc] initWithRootViewController:viewController];
    XCTAssertEqual(nc.viewControllers.count, 1);
    [viewController tableView:viewController.tableView
      didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    XCTAssertEqual(nc.viewControllers.count, 2);
    XCTAssertEqualObjects(nc.topViewController.class, ContactViewController.class);
    ContactViewController* cvc = (ContactViewController*) nc.topViewController;
    XCTAssertEqualObjects(cvc.contactID, @"1");
}

@end
