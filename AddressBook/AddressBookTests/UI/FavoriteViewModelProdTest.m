//
//  FavoriteViewModelProdTest.m
//  AddressBook
//
//  Created by Marko Cicak on 4/24/15.
//  Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import "FavoriteViewModelProd.h"
#import "Contact.h"
#import "ContactDao.h"
#import "FavoriteViewController.h"

@interface FavoriteViewModelProdTest : XCTestCase
{
    FavoriteViewModelProd* model;
}
@end

@implementation FavoriteViewModelProdTest

- (void) setUp
{
    [super setUp];
    [self prepareData];
    model = [[FavoriteViewModelProd alloc] init];
}

- (void) tearDown
{
    model = nil;
    [super tearDown];
}

- (void) testRowsCountIsCorrect
{
    UIStoryboard* s = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    FavoriteViewController* fvc = [s instantiateViewControllerWithIdentifier:@"FavoritesSID"];

    XCTAssertEqual([model tableView:nil numberOfRowsInSection:0], 3);
    NSIndexPath* path = [NSIndexPath indexPathForRow:0 inSection:0];
    UITableViewCell* cell = [model tableView:fvc.tableView cellForRowAtIndexPath:path];
    XCTAssertEqualObjects(cell.textLabel.text, @"George Harrison");
    XCTAssertEqualObjects(cell.detailTextLabel.text, @"London");
    XCTAssertEqual(cell.accessoryType, UITableViewCellAccessoryDetailButton);
}

- (void) testRemovingFavoriteContact
{
    NSIndexPath* path = [NSIndexPath indexPathForRow:0 inSection:0];
    Contact* contact = [model contactAtIndex:0];
    XCTAssertTrue(contact.favorite.boolValue);
    [model tableView:nil commitEditingStyle:UITableViewCellEditingStyleDelete forRowAtIndexPath:path];
    XCTAssertFalse(contact.favorite.boolValue);
}

- (void) prepareData
{
    SREG.coredata = nil;
    ContactDao* contactDao = [[ContactDao alloc] init];
    Contact* c1 = [contactDao insertNew];
    c1.contactID = @"1";
    c1.firstName = @"George";
    c1.lastName = @"Harrison";
    c1.city = @"London";
    c1.favorite = @(YES);

    Contact* c2 = [contactDao insertNew];
    c2.contactID = @"2";
    c2.firstName = @"John";
    c2.lastName = @"Lennon";
    c2.favorite = @(YES);

    Contact* c3 = [contactDao insertNew];
    c3.contactID = @"3";
    c3.firstName = @"George";
    c3.lastName = @"Martin";
    c3.favorite = @(YES);

    Contact* c4 = [contactDao insertNew];
    c4.contactID = @"4";
    c4.firstName = @"Paul";
    c4.lastName = @"McCartney";

    Contact* c5 = [contactDao insertNew];
    c5.contactID = @"5";
    c5.firstName = @"Ringo";
    c5.lastName = @"Starr";

    [contactDao save];
}

@end
