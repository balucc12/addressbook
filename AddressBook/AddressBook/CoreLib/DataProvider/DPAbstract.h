//
// Created by Marko Cicak on 4/7/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AFHTTPRequestOperation;

typedef void(^SuccessBlock)(AFHTTPRequestOperation*, id);

typedef void(^FailureBlock)(AFHTTPRequestOperation*, NSError*);

@interface DPAbstract : NSObject

@property(nonatomic, assign) BOOL inProgress;

@property(nonatomic, strong) AFHTTPRequestOperationManager* client;

- (NSString*) path;

- (id) params;

- (void) execute;

- (SuccessBlock) successBlock;

- (FailureBlock) failureBlock;

@end
