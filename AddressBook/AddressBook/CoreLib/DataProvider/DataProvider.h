//
// Created by Marko Cicak on 10/27/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

@protocol DataProvider <NSObject>

- (void) retrieveAllContacts;

- (void) retrieveContactWithID:(NSString*)contactID;

@end
