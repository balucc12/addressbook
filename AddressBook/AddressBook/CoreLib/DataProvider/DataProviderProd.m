//
// Created by Marko Cicak on 4/7/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import "DataProvider.h"
#import "DataProviderProd.h"
#import "DPAbstract.h"
#import "AFHTTPRequestOperationManager.h"
#import "DPAllContacts.h"
#import "DPContact.h"

@interface DataProviderProd ()
@property(nonatomic, strong) NSMutableArray* restActions;
@property(nonatomic, strong) NSPredicate* inProgressPredicate;
@property(nonatomic, strong) AFHTTPRequestOperationManager* apiRestClient;
@end


@implementation DataProviderProd

- (instancetype) init
{
    self = [super init];
    if (self)
    {
        _restActions = [NSMutableArray array];
        _inProgressPredicate = [NSPredicate predicateWithFormat:@"inProgress == %@", @(YES)];
    }
    return self;
}

- (void) retrieveAllContacts
{
    DPAllContacts* provider = [[DPAllContacts alloc] init];
    [self executeRestAction:provider];
}

- (void) retrieveContactWithID:(NSString*)contactID
{
    DPContact* provider = [[DPContact alloc] initWithContactID:contactID];
    [self executeRestAction:provider];
}

#pragma mark - Common

- (BOOL) executeRestAction:(DPAbstract*)action
{
    // clear completed data providers
    [self.restActions filterUsingPredicate:self.inProgressPredicate];

    if (![self.restActions containsObject:action])
    {
        [self.restActions addObject:action];
        action.inProgress = YES;
        action.client = self.apiRestClient;
        [action execute];
        return YES;
    }
    else
    {
        return NO;
    }
}

- (AFHTTPRequestOperationManager*) apiRestClient
{
    if (!_apiRestClient)
    {
        NSString* pathAPI = [NSString stringWithFormat:@"%@/", kCCBaseUrl];
        _apiRestClient = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:pathAPI]];
    }
    return _apiRestClient;
}

@end
