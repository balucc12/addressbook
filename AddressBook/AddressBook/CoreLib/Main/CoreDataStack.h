//
// Created by Marko Cicak on 4/1/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CoreDataStack : NSObject

@property(readonly, strong, nonatomic) NSManagedObjectContext* managedObjectContext;
@property(readonly, strong, nonatomic) NSManagedObjectModel* managedObjectModel;
@property(readonly, strong, nonatomic) NSPersistentStoreCoordinator* persistentStoreCoordinator;

// Call this method from whatever thread.
- (NSManagedObjectContext*) createContext;

@end
