//
// Created by Marko Cicak on 4/1/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

@interface CoreDataStack ()
@property(strong, nonatomic) NSManagedObjectContext* managedObjectContext;
@property(strong, nonatomic) NSManagedObjectModel* managedObjectModel;
@property(strong, nonatomic) NSPersistentStoreCoordinator* persistentStoreCoordinator;
@end


@implementation CoreDataStack

#pragma mark - Core Data Stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext*) managedObjectContext
{
    if (_managedObjectContext != nil)
    {
        return _managedObjectContext;
    }

    NSPersistentStoreCoordinator* coordinator = self.persistentStoreCoordinator;
    if (coordinator != nil)
    {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        _managedObjectContext.persistentStoreCoordinator = coordinator;
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel*) managedObjectModel
{
    if (!_managedObjectModel)
    {
        NSBundle* bundle = [NSBundle mainBundle];
        NSURL* modelURL = [bundle URLForResource:@"_Database" withExtension:@"momd"];
        _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    }
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator*) persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil)
    {
        return _persistentStoreCoordinator;
    }

    // Force all threads that reach this
    // code to be processed in an ordered manner on the main thread.  The first
    // one will initialize the data, and the rest will just return with that
    // data.  However, it ensures the creation is not attempted multiple times.
    if (![NSThread currentThread].isMainThread)
    {
        dispatch_sync(dispatch_get_main_queue(), ^{
            (void) [self persistentStoreCoordinator];
        });
        return _persistentStoreCoordinator;
    }

    if (![[NSFileManager defaultManager] fileExistsAtPath:[self databasePath]])
    {
        NSError* error = NULL;
        [[NSFileManager defaultManager] createDirectoryAtURL:SREG.context.applicationDocumentsDirectoryURL
                                 withIntermediateDirectories:YES
                                                  attributes:nil
                                                       error:&error];
        if (error)
        {
            NSLog(@"Could not create tenant base document directory %@, because %@",
                  SREG.context.applicationDocumentsDirectoryURL, error);
        }
    }

    NSURL* storeURL = [SREG.context.applicationDocumentsDirectoryURL URLByAppendingPathComponent:@"Database.sqlite"];
    NSDictionary* options = @{ NSMigratePersistentStoresAutomaticallyOption : @(YES),
                               NSInferMappingModelAutomaticallyOption : @(YES) };

    NSError* error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc]
            initWithManagedObjectModel:self.managedObjectModel];
    if (ANIMATED)
    {
        [_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                  configuration:nil URL:storeURL options:options error:&error];
    }
    else // create in memory store when executing unit tests
    {
        [_persistentStoreCoordinator addPersistentStoreWithType:NSInMemoryStoreType
                                                  configuration:nil URL:nil options:nil error:&error];
    }

    if (error)
    {
        NSLog(@"Unresolved error while migrating database: %@, %@", error, error.userInfo);
        abort();
    }

    return _persistentStoreCoordinator;
}

- (NSManagedObjectContext*) createContext
{
    NSManagedObjectContext* ret = [[NSManagedObjectContext alloc] init];
    ret.persistentStoreCoordinator = self.persistentStoreCoordinator;
    return ret;
}

- (NSString*) databasePath
{
    NSString* appDocDir = [SREG.context.applicationDocumentsDirectoryURL.relativePath
            stringByAppendingPathComponent:@"_Database.sqlite"];
    return appDocDir;
}

@end
