//
// Created by Marko Cicak on 4/1/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import "DataProviderProd.h"

static ServiceRegistry* _instance = nil;

@implementation ServiceRegistry

+ (ServiceRegistry*) instance
{
    if (!_instance)
    {
        _instance = [[self alloc] init];
    }
    return _instance;
}

- (CoreDataStack*) coredata
{
    if (!_coredata)
    {
        _coredata = CoreDataStack.new;
    }
    return _coredata;
}

- (Context*) context
{
    if (!_context)
    {
        _context = Context.new;
    }
    return _context;
}

- (id <DataProvider>) dataProvider
{
    if (!_dataProvider)
    {
        _dataProvider = DataProviderProd.new;
    }
    return _dataProvider;
}

@end
