//
// Created by Marko Cicak on 4/7/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import "ContactDao.h"


@implementation ContactDao

- (instancetype) init
{
    self = [super init];
    if (self)
    {
        self.entityName = @"Contact";
        self.attributeID = @"contactID";
        self.defaultOrderBy = [NSSortDescriptor sortDescriptorWithKey:@"lastName"
                                                            ascending:YES
                                                             selector:@selector(localizedCaseInsensitiveCompare:)];
    }
    return self;
}

- (NSFetchRequest*) requestForQuery:(NSString*)query
{
    NSFetchRequest* request = self.fetchRequest;
    request.sortDescriptors = @[
            [NSSortDescriptor sortDescriptorWithKey:@"lastName" ascending:YES selector:@selector(caseInsensitiveCompare:)],
            [NSSortDescriptor sortDescriptorWithKey:@"firstName" ascending:YES selector:@selector(caseInsensitiveCompare:)]
    ];
    if (query.length > 0)
    {
        request.predicate = [NSPredicate predicateWithFormat:@"lastName contains[c] %@ OR firstName contains[c] %@",
                                                             query, query];
    }

    return request;
}

- (NSFetchRequest*) requestForFavoriteContacts
{
    NSFetchRequest* ret = self.fetchRequest;
    ret.predicate = [NSPredicate predicateWithFormat:@"favorite = YES"];
    return ret;
}

@end
