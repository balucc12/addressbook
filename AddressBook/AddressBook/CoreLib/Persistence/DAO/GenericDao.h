//
// Created by Marko Cicak
//
//  Copyright (c) 2014 ITEngine d.o.o. All rights reserved.
//

@interface GenericDao : NSObject

@property(nonatomic, strong) NSManagedObjectContext* managedObjectContext;
@property(nonatomic, strong) NSEntityDescription* entityDescription;
@property(nonatomic, strong) NSString* entityName;
@property(nonatomic, strong) NSSortDescriptor* defaultOrderBy;

/* Name of attribute representing ID of managed object. */
@property(nonatomic, strong) NSString* attributeID;

#pragma mark - Init methods

/*
    Create object with given NSManagedObjectContext.
 */
- (id) initWithManagedObjectContext:(NSManagedObjectContext*)moc;

#pragma mark - Fetch utility methods

- (NSFetchRequest*) fetchRequest;

- (NSArray*) fetchWithPredicate:(NSPredicate*)predicate andSortDescriptors:(NSArray*)sortDescriptors;

- (BOOL) isTableEmpty;

#pragma mark - CRUD methods

- (id) insertNew;

/*
    Returns managed object if one is found with given ID, otherwise newly created managed object with ID set.
 */
- (id) getExistingOrNew:(id)entityID;

/*
    Returns all CoreData Managed Objects in table.
 */
- (NSArray*) findAll;

/*
    Returns managed object if one is found with given ID, otherwise nil.
    @param id entityID - Entity ID field, usually NSString, but can be NSNumber or any CoreData supported type.
*/
- (id) findByID:(id)entityID;

/*
 * Creates IN query and fetch existing objects from database.
 * Fetched entities are sorted by sortDescriptors parameter.
 *
 * @return NSArray of existing Core Data objects
 */
- (NSArray*) findAllByIDs:(NSArray*)entityIDs sortDescriptors:(NSArray*)sortDescriptors;

/*
    Returns requested properties of entities by given IDs, sorted by sort descriptors.
    @param entityIDs - entity IDs to fetch
    @param sortDescriptors - array of strings representing fields in CoreData table
    @param properties - array of strings representing properties to fetch
    @return array of fetched entities
 */
- (NSArray*) findAllByIDs:(NSArray*)entityIDs sortDescriptors:(NSArray*)sortDescriptors propertiesToFetch:(NSArray*)properties;

/*
    If object doesn't exist yet, new one will be inserted, otherwise existing will be updated.

    @param id domainObject - Domain object, ie CDDocument
    @param BOOL save - should Managed Object Context be saved after storing object into CoreData
 */
- (void) putDomainObject:(id)domainObject andSave:(BOOL)save;

/*
 * This method to following steps:
 * 1) Create array of id's from entities fetched from the Rest server
 * 2) Sort the id's in ascending order
 * 3) Create IN query and fetch existing entities from database
 * 4) Iterate through fetched entities and update or delete them
 * 5) Insert new entities in core data
 *
 * @param id domainObject - Domain object, ie CDDocument
 * @param BOOL save - should Managed Object Context be saved after storing object into CoreData
 */
- (void) updateOrCreateNew:(NSArray*)domainObjects andSave:(BOOL)save;

/*
    Creates NSArray of id's from passed domain objects.
 */
- (NSArray*) entityIDsFromDomainObjects:(NSArray*)domainObjects;

/*
 * Returns sorted array of strings using compare: or caseInsensitiveCompare: methods
 */
- (NSArray*) sortedArrayUsingEntityIDs:(NSArray*)entityIDs caseInsensitive:(BOOL)caseInsensitive;

/*
 * Creates IN query and fetch existing objects from database
 * Fetched entities are sorted by attributeID.
 * Order of entities depends on sortAscending parameter.
 *
 * @return NSArray of existing Core Data objects
 */
- (NSArray*) entitiesByIDs:(NSArray*)entityIDs sortAscending:(BOOL)ascending;

/* Deletes NSManagedObject from table. */
- (BOOL) delete:(id)entity andSave:(BOOL)save;

- (BOOL) deleteEntities:(NSArray*)entities andSave:(BOOL)save;

/* Deletes all table records. */
- (BOOL) deleteAll;

#pragma mark - Context save

/* Saves managed object context to store. */
- (BOOL) save;

#pragma mark - Data transition methods

/*
    Transforms domain object to CoreData model.

    @param id domainObject - CD Domain object, ie, CDDocument
    @param id managedObject - CoreData NSManagedObject object, ie CoreDataDocument
 */
- (void) domain:(id)domainObject toCoreDataModel:(id)managedObject;

/*
    Transforms CoreData object to domain object.

    @param id managedObject - NSManagedObject object, ie CoreDataDocument
    @param id domainObject - CD Domain object, ie CDDocument
 */
- (void) coreDataModel:(id)managedObject toDomain:(id)domainObject;

@end
