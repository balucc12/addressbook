//
// Created by Marko Cicak on 4/7/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import "EmailDao.h"


@implementation EmailDao

- (instancetype) init
{
    self = [super init];
    if (self)
    {
        self.entityName = @"Email";
        self.attributeID = @"address";
        self.defaultOrderBy = [NSSortDescriptor sortDescriptorWithKey:@"address"
                                                            ascending:YES
                                                             selector:@selector(caseInsensitiveCompare:)];
    }
    return self;
}

@end
