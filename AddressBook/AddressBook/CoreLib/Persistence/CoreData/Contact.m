//
//  Contact.m
//  AddressBook
//
//  Created by Marko Cicak on 4/8/15.
//  Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import "Contact.h"
#import "Email.h"


@implementation Contact

@dynamic city;
@dynamic contactID;
@dynamic firstName;
@dynamic lastName;
@dynamic phone;
@dynamic state;
@dynamic street;
@dynamic zip;
@dynamic favorite;
@dynamic emails;

@end
