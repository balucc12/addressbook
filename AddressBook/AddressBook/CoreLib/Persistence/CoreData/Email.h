//
//  Email.h
//  AddressBook
//
//  Created by Marko Cicak on 4/8/15.
//  Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Contact;

@interface Email : NSManagedObject

@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) Contact *contact;

@end
