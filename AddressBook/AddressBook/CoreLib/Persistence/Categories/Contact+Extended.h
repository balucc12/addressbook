//
// Created by Marko Cicak on 4/10/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Contact.h"

@interface Contact (Extended)

- (NSString*) lastnameFirstLetter;

- (NSString*) fullname;

@end
