//
// Created by Marko Cicak on 4/2/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import "FavoriteViewController.h"
#import "FavoriteViewModel.h"
#import "FavoriteViewModelMock.h"
#import "Contact.h"
#import "FavoriteViewModelProd.h"
#import "ContactsViewController.h"
#import "ContactViewController.h"

@interface FavoriteViewController () <NSFetchedResultsControllerDelegate>
@property(nonatomic, strong) id <FavoriteViewModel> model;
@end

@implementation FavoriteViewController

- (void) viewDidLoad
{
    [super viewDidLoad];
    self.tableView.dataSource = self.model;
    [self.model setFetchedResultsControllerDelegate:self];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

#pragma mark - UITableViewDelegate

- (void) tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath
{
    Contact* contact = [self.model contactAtIndex:indexPath.row];
    if (contact.phone.length > 0)
    {
        NSString* phoneNumber = [@"tel://" stringByAppendingString:contact.phone];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
    }
    else
    {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                                                                       message:@"Contact doesn't have phone number."
                                                                preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:ANIMATED completion:nil];
    }
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void) tableView:(UITableView*)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath*)indexPath
{
    Contact* contact = [self.model contactAtIndex:indexPath.row];
    ContactViewController* cvc = [[ContactViewController alloc] initWithStyle:UITableViewStyleGrouped];
    cvc.contactID = contact.contactID;
    [self.navigationController pushViewController:cvc animated:ANIMATED];
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void) controllerWillChangeContent:(NSFetchedResultsController*)controller
{
    [self.tableView beginUpdates];
}

- (void) controller:(NSFetchedResultsController*)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
            atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch (type)
    {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationNone];
            break;

        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationNone];
            break;
        case NSFetchedResultsChangeMove:
        case NSFetchedResultsChangeUpdate:
            break;
    }
}

- (void) controller:(NSFetchedResultsController*)controller didChangeObject:(id)anObject
        atIndexPath:(NSIndexPath*)indexPath forChangeType:(NSFetchedResultsChangeType)type
       newIndexPath:(NSIndexPath*)newIndexPath
{
    UITableView* tableView = self.tableView;

    indexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
    newIndexPath = [NSIndexPath indexPathForRow:newIndexPath.row inSection:newIndexPath.section];

    switch (type)
    {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[ newIndexPath ] withRowAnimation:UITableViewRowAnimationNone];
            break;

        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[ indexPath ] withRowAnimation:UITableViewRowAnimationNone];
            break;

        case NSFetchedResultsChangeUpdate:
            [tableView reloadRowsAtIndexPaths:@[ indexPath ] withRowAnimation:UITableViewRowAnimationNone];
            break;

        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[ indexPath ] withRowAnimation:UITableViewRowAnimationNone];
            [tableView insertRowsAtIndexPaths:@[ newIndexPath ] withRowAnimation:UITableViewRowAnimationNone];
            break;
    }
}

- (void) controllerDidChangeContent:(NSFetchedResultsController*)controller
{
    [self.tableView endUpdates];
}

#pragma mark - Actions

- (IBAction) didTapAddButton:(id)sender
{
    UINavigationController* nc = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactsNavSID"];
    nc.topViewController.navigationItem.prompt = @"Choose a contact to add to Favorites";
    ((ContactsViewController*) nc.topViewController).pickingMode = YES;
    [self presentViewController:nc animated:ANIMATED completion:nil];
}

- (IBAction) didTapEditButton:(id)sender
{
    [self.tableView setEditing:!self.tableView.editing animated:ANIMATED];
}

#pragma mark - Getters

- (id <FavoriteViewModel>) model
{
    if (!_model)
    {
        _model = [[FavoriteViewModelProd alloc] init];
    }
    return _model;
}

@end
