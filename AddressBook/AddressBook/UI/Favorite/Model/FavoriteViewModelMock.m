//
// Created by Marko Cicak on 4/20/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import "FavoriteViewModelMock.h"
#import "Contact.h"
#import "ContactDao.h"
#import "UITableViewCell+Contact.h"


@implementation FavoriteViewModelMock
{
    NSArray* data;
}

- (instancetype) init
{
    self = [super init];
    if (self)
    {
        [self initData];
    }
    return self;
}

- (NSInteger) tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section
{
    return data.count;
}

- (UITableViewCell*) tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"FavoriteCell" forIndexPath:indexPath];
    Contact* contact = [self contactAtIndex:indexPath.row];
    [cell configureWithFavoriteContact:contact];
    return cell;
}

- (Contact*) contactAtIndex:(NSInteger)idx
{
    return data[(NSUInteger) idx];
}

- (void) setFetchedResultsControllerDelegate:(id <NSFetchedResultsControllerDelegate>)delegate
{

}

#pragma mark - Private

- (void) initData
{
    ContactDao* contactDao = [[ContactDao alloc] init];
    Contact* c1 = [contactDao insertNew];
    c1.contactID = @"1";
    c1.firstName = @"George";
    c1.lastName = @"Harrison";
    c1.city = @"Novi Sad";
    c1.phone = @"+381637262835";
    c1.favorite = @(YES);

    Contact* c2 = [contactDao insertNew];
    c2.contactID = @"2";
    c2.firstName = @"John";
    c2.lastName = @"Lennon";
    c2.city = @"Belgrade";
    c2.phone = nil;
    c2.favorite = @(YES);

    Contact* c3 = [contactDao insertNew];
    c3.contactID = @"3";
    c3.firstName = @"George";
    c3.lastName = @"Martin";
    c3.city = @"Budampest";
    c3.phone = @"+381637262831";
    c3.favorite = @(YES);

    Contact* c4 = [contactDao insertNew];
    c4.contactID = @"4";
    c4.firstName = @"Paul";
    c4.lastName = @"McCartney";
    c4.phone = @"+381637262835";
    c4.city = @"London";

    Contact* c5 = [contactDao insertNew];
    c5.contactID = @"5";
    c5.firstName = @"Ringo";
    c5.lastName = @"Starr";
    c5.phone = @"+381637262836";
    c5.city = @"Vienna";

    [contactDao save];

    data = @[ c1, c2, c3, c4, c5 ];
}

@end
