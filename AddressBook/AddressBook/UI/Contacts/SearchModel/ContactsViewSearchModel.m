//
// Created by Marko Cicak on 4/10/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import "ContactsViewSearchModel.h"
#import "ContactDao.h"
#import "Contact.h"
#import "UITableViewCell+Contact.h"

@interface ContactsViewSearchModel ()
@property(nonatomic, strong) NSFetchedResultsController* fetchedResultsController;
@property(nonatomic, strong) ContactDao* contactDao;
@end


@implementation ContactsViewSearchModel

- (NSInteger) tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = self.fetchedResultsController.sections[(NSUInteger) section];
    return sectionInfo.numberOfObjects;
}

- (UITableViewCell*) tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    static NSString* CellIdentifier = @"SearchCell";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell)
    {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    Contact* contact = [self contactAtIndexPath:indexPath];
    [cell configureWithContact:contact];
    return cell;
}

- (Contact*) contactAtIndexPath:(NSIndexPath*)path
{
    return [self.fetchedResultsController objectAtIndexPath:path];
}

- (void) setQuery:(NSString*)query
{
    _query = query;
    _fetchedResultsController = nil;
}

#pragma mark - Getters

- (NSFetchedResultsController*) fetchedResultsController
{
    if (!_fetchedResultsController)
    {
        NSFetchRequest* request = [self.contactDao requestForQuery:self.query];
        [request setFetchBatchSize:50];

        NSFetchedResultsController* aFetchedResultsController = [[NSFetchedResultsController alloc]
                initWithFetchRequest:request
                managedObjectContext:SREG.coredata.managedObjectContext
                  sectionNameKeyPath:nil
                           cacheName:nil];
        _fetchedResultsController = aFetchedResultsController;

        NSError* error;
        if (![_fetchedResultsController performFetch:&error])
        {
            NSLog(@"ERROR: %@", error.localizedDescription);
            abort();
        }
    }
    return _fetchedResultsController;
}

- (ContactDao*) contactDao
{
    if (!_contactDao)
    {
        _contactDao = [[ContactDao alloc] init];
    }
    return _contactDao;
}

@end
