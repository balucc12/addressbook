//
// Created by Marko Cicak on 4/8/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Contact;

@protocol ContactsViewModel <UITableViewDataSource>

- (Contact*) contactAtIndexPath:(NSIndexPath*)path;

- (void) setFRCDelegate:(id <NSFetchedResultsControllerDelegate>)controller;

@end
