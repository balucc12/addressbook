//
// Created by Marko Cicak on 4/8/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ContactsViewModel.h"


@interface ContactsViewModelProd : NSObject <ContactsViewModel>
@end
