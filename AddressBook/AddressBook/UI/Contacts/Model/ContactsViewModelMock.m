//
// Created by Marko Cicak on 4/8/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import "ContactsViewModelMock.h"
#import "ContactDao.h"
#import "Contact.h"
#import "UITableViewCell+Contact.h"


@implementation ContactsViewModelMock
{
    NSDictionary* data;
    NSArray* keys;
}


- (instancetype) init
{
    self = [super init];
    if (self)
    {
        [self initData];
    }
    return self;
}

#pragma mark - UITableViewDataSource

- (NSInteger) numberOfSectionsInTableView:(UITableView*)tableView
{
    return data.count;
}

- (NSInteger) tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString* key = [self sectionIndexTitlesForTableView:nil][(NSUInteger) section];
    return [data[key] count];
}

- (UITableViewCell*) tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"ContactCell" forIndexPath:indexPath];
    NSString* key = [self sectionIndexTitlesForTableView:nil][(NSUInteger) indexPath.section];
    Contact* contact = data[key][(NSUInteger) indexPath.row];
    [cell configureWithContact:contact];
    return cell;
}

- (NSString*) tableView:(UITableView*)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString* key = [self sectionIndexTitlesForTableView:nil][(NSUInteger) section];
    return key;
}

- (NSArray*) sectionIndexTitlesForTableView:(UITableView*)tableView
{
    return keys;
}

- (NSInteger) tableView:(UITableView*)tableView sectionForSectionIndexTitle:(NSString*)title atIndex:(NSInteger)index
{
    return index;
}

- (Contact*) contactAtIndexPath:(NSIndexPath*)path
{
    return data[keys[(NSUInteger) path.section]][(NSUInteger) path.row];
}

- (void) setFRCDelegate:(id <NSFetchedResultsControllerDelegate>)controller
{
    self.hasFRCDelegate = controller != nil;
}

#pragma mark - Private

- (void) initData
{
    ContactDao* contactDao = [[ContactDao alloc] init];
    Contact* c1 = [contactDao insertNew];
    c1.contactID = @"1";
    c1.firstName = @"George";
    c1.lastName = @"Harrison";

    Contact* c2 = [contactDao insertNew];
    c2.contactID = @"2";
    c2.firstName = @"John";
    c2.lastName = @"Lennon";

    Contact* c3 = [contactDao insertNew];
    c3.contactID = @"3";
    c3.firstName = @"George";
    c3.lastName = @"Martin";

    Contact* c4 = [contactDao insertNew];
    c4.contactID = @"4";
    c4.firstName = @"Paul";
    c4.lastName = @"McCartney";

    Contact* c5 = [contactDao insertNew];
    c5.contactID = @"5";
    c5.firstName = @"Ringo";
    c5.lastName = @"Starr";

    [contactDao save];

    data = @{
            @"H" : @[ c1 ],
            @"L" : @[ c2 ],
            @"M" : @[ c3, c4 ],
            @"S" : @[ c5 ]
    };

    keys = [data.allKeys sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
}

@end
