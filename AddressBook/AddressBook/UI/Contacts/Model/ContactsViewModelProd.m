//
// Created by Marko Cicak on 4/8/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import "ContactsViewModelProd.h"
#import "Contact.h"
#import "UITableViewCell+Contact.h"
#import "ContactDao.h"

@interface ContactsViewModelProd ()
@property(nonatomic, strong) NSFetchedResultsController* fetchedResultsController;
@property(nonatomic, strong) ContactDao* contactDao;
@end


@implementation ContactsViewModelProd

- (void) setFRCDelegate:(id <NSFetchedResultsControllerDelegate>)controller
{
    self.fetchedResultsController.delegate = controller;
}

#pragma mark - UITableViewDataSource

- (NSInteger) numberOfSectionsInTableView:(UITableView*)tableView
{
    return self.fetchedResultsController.sections.count;
}

- (NSInteger) tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = self.fetchedResultsController.sections[(NSUInteger) section];
    return sectionInfo.numberOfObjects;
}

- (UITableViewCell*) tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"ContactCell" forIndexPath:indexPath];
    Contact* contact = [self contactAtIndexPath:indexPath];
    [cell configureWithContact:contact];
    return cell;
}

- (NSString*) tableView:(UITableView*)tableView titleForHeaderInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = self.fetchedResultsController.sections[(NSUInteger) section];
    return sectionInfo.name;
}

- (NSArray*) sectionIndexTitlesForTableView:(UITableView*)tableView
{
    return self.fetchedResultsController.sectionIndexTitles;
}

- (NSInteger) tableView:(UITableView*)tableView sectionForSectionIndexTitle:(NSString*)title atIndex:(NSInteger)index
{
    return index;
}

- (Contact*) contactAtIndexPath:(NSIndexPath*)path
{
    return [self.fetchedResultsController objectAtIndexPath:path];
}

#pragma mark - Getters

- (NSFetchedResultsController*) fetchedResultsController
{
    if (!_fetchedResultsController)
    {
        NSFetchRequest* request = self.contactDao.fetchRequest;
        request.sortDescriptors = @[
                [NSSortDescriptor sortDescriptorWithKey:@"lastName" ascending:YES selector:@selector(caseInsensitiveCompare:)],
                [NSSortDescriptor sortDescriptorWithKey:@"firstName" ascending:YES selector:@selector(caseInsensitiveCompare:)]
        ];

        [request setFetchBatchSize:50];

        NSFetchedResultsController* aFetchedResultsController = [[NSFetchedResultsController alloc]
                initWithFetchRequest:request
                managedObjectContext:SREG.coredata.managedObjectContext
                  sectionNameKeyPath:@"lastnameFirstLetter"
                           cacheName:nil];
        _fetchedResultsController = aFetchedResultsController;

        NSError* error;
        if (![_fetchedResultsController performFetch:&error])
        {
            NSLog(@"ERROR: %@", error.localizedDescription);
            abort();
        }
    }
    return _fetchedResultsController;
}

- (ContactDao*) contactDao
{
    if (!_contactDao)
    {
        _contactDao = [[ContactDao alloc] init];
    }
    return _contactDao;
}

@end
