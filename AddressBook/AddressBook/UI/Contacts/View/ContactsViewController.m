//
// Created by Marko Cicak on 4/2/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import "ContactsViewController.h"
#import "DataProvider.h"
#import "ContactsViewModel.h"
#import "ContactsViewModelMock.h"
#import "Contact.h"
#import "ContactsViewModelProd.h"
#import "ContactsViewSearchModel.h"
#import "ContactViewController.h"

@interface ContactsViewController () <UISearchDisplayDelegate, NSFetchedResultsControllerDelegate>
@property(nonatomic, strong) id <ContactsViewModel> model;
@property(nonatomic, strong) ContactsViewSearchModel* searchModel;
@end

@implementation ContactsViewController

- (void) viewDidLoad
{
    [super viewDidLoad];
    if (self.pickingMode)
    {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
                initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self
                                     action:@selector(didTapCancelButton)];
    }
    self.tableView.dataSource = self.model;
    [self.model setFRCDelegate:self];
    self.searchDisplayController.searchResultsDataSource = self.searchModel;
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [SREG.dataProvider retrieveAllContacts];
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void) controllerWillChangeContent:(NSFetchedResultsController*)controller
{
    [self.tableView beginUpdates];
}

- (void) controller:(NSFetchedResultsController*)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
            atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch (type)
    {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationNone];
            break;

        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationNone];
            break;
        case NSFetchedResultsChangeMove:
        case NSFetchedResultsChangeUpdate:
            break;
    }
}

- (void) controller:(NSFetchedResultsController*)controller didChangeObject:(id)anObject
        atIndexPath:(NSIndexPath*)indexPath forChangeType:(NSFetchedResultsChangeType)type
       newIndexPath:(NSIndexPath*)newIndexPath
{
    UITableView* tableView = self.tableView;

    indexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
    newIndexPath = [NSIndexPath indexPathForRow:newIndexPath.row inSection:newIndexPath.section];

    switch (type)
    {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[ newIndexPath ] withRowAnimation:UITableViewRowAnimationNone];
            break;

        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[ indexPath ] withRowAnimation:UITableViewRowAnimationNone];
            break;

        case NSFetchedResultsChangeUpdate:
            [tableView reloadRowsAtIndexPaths:@[ indexPath ] withRowAnimation:UITableViewRowAnimationNone];
            break;

        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[ indexPath ] withRowAnimation:UITableViewRowAnimationNone];
            [tableView insertRowsAtIndexPaths:@[ newIndexPath ] withRowAnimation:UITableViewRowAnimationNone];
            break;
    }
}

- (void) controllerDidChangeContent:(NSFetchedResultsController*)controller
{
    [self.tableView endUpdates];
}

#pragma mark - UITableViewDelegate

- (void) tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath
{
    Contact* contact;
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        contact = [self.searchModel contactAtIndexPath:indexPath];
    }
    else
    {
        contact = [self.model contactAtIndexPath:indexPath];
    }
    if (self.pickingMode)
    {
        contact.favorite = @(YES);
        [contact.managedObjectContext save:nil];
        [SREG.dataProvider retrieveContactWithID:contact.contactID];
        [self dismissViewControllerAnimated:ANIMATED completion:nil];
    }
    else
    {
        ContactViewController* cvc = [[ContactViewController alloc] initWithStyle:UITableViewStyleGrouped];
        cvc.contactID = contact.contactID;
        [self.navigationController pushViewController:cvc animated:ANIMATED];
    }
}

#pragma mark - UISearchDisplayDelegate

- (BOOL) searchDisplayController:(UISearchDisplayController*)controller
shouldReloadTableForSearchString:(NSString*)searchString
{
    self.searchModel.query = searchString;
    return YES;
}

#pragma mark - Actions

- (void) didTapCancelButton
{
    [self dismissViewControllerAnimated:ANIMATED completion:nil];
}

#pragma mark - Getters

- (id <ContactsViewModel>) model
{
    if (!_model)
    {
        _model = [[ContactsViewModelProd alloc] init];
//        _model = [[ContactsViewModelMock alloc] init];
    }
    return _model;
}

- (ContactsViewSearchModel*) searchModel
{
    if (!_searchModel)
    {
        _searchModel = [[ContactsViewSearchModel alloc] init];
    }
    return _searchModel;
}

@end
