//
// Created by Marko Cicak on 4/8/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Contact;

@interface UITableViewCell (Contact)
- (void) configureWithContact:(Contact*)contact;

- (void) configureWithFavoriteContact:(Contact*)contact;
@end
