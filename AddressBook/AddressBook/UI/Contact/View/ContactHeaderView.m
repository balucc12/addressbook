//
// Created by Marko Cicak on 4/18/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import "ContactHeaderView.h"

@implementation ContactHeaderView

- (instancetype) init
{
    self = [super init];
    if (self)
    {
        [self initialize];
    }

    return self;
}

- (instancetype) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initialize];
    }

    return self;
}

- (id) initWithCoder:(NSCoder*)coder
{
    self = [super initWithCoder:coder];
    if (self)
    {
        [self initialize];
    }

    return self;
}

- (void) initialize
{
    [self addSubview:self.titleLabel];
    NSDictionary* views = @{ @"label" : self.titleLabel };
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-16-[label]-16-|"
                                                                 options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-16-[label]"
                                                                 options:0 metrics:nil views:views]];
}

#pragma mark - Getters

- (UILabel*) titleLabel
{
    if (!_titleLabel)
    {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont boldSystemFontOfSize:_titleLabel.font.pointSize];
        _titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _titleLabel;
}

@end
