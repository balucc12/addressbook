//
// Created by Marko Cicak on 4/18/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ContactHeaderView : UIView
@property(nonatomic, strong) UILabel* titleLabel;
@end
