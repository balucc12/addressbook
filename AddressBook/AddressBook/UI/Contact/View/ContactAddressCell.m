//
// Created by Marko Cicak on 4/18/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import "ContactAddressCell.h"


@implementation ContactAddressCell

- (instancetype) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        [self setupInstance];
    }

    return self;
}

- (void) setupInstance
{
    [self addSubview:self.streetLabel];
    [self addSubview:self.cityLabel];
    [self addSubview:self.stateLabel];

    NSDictionary* views = @{
            @"street" : self.streetLabel,
            @"city" : self.cityLabel,
            @"state" : self.stateLabel
    };
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-16-[street]-16-|"
                                                                 options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-16-[city]-16-|"
                                                                 options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-16-[state]-16-|"
                                                                 options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-8-[street]-8-[city]-8-[state]-8-|"
                                                                 options:0 metrics:nil views:views]];
}

#pragma mark - Getters

- (UILabel*) streetLabel
{
    if (!_streetLabel)
    {
        _streetLabel = [[UILabel alloc] init];
        _streetLabel.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _streetLabel;
}

- (UILabel*) cityLabel
{
    if (!_cityLabel)
    {
        _cityLabel = [[UILabel alloc] init];
        _cityLabel.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _cityLabel;
}

- (UILabel*) stateLabel
{
    if (!_stateLabel)
    {
        _stateLabel = [[UILabel alloc] init];
        _stateLabel.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _stateLabel;
}

@end
