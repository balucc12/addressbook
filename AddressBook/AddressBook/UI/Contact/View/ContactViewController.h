//
// Created by Marko Cicak on 4/13/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ContactViewController : UITableViewController
@property(nonatomic, copy) NSString* contactID;
@end
