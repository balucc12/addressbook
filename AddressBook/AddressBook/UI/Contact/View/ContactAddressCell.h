//
// Created by Marko Cicak on 4/18/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ContactAddressCell : UITableViewCell
@property(nonatomic, strong) UILabel* streetLabel;
@property(nonatomic, strong) UILabel* cityLabel;
@property(nonatomic, strong) UILabel* stateLabel;
@end
