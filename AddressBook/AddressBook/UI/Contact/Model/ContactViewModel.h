//
// Created by Marko Cicak on 4/18/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Contact;

@protocol ContactViewModel <NSObject>

- (NSInteger) emailsCount;

- (NSString*) fullname;

- (NSString*) phone;

- (NSString*) street;

- (NSString*) state;

- (NSString*) city;

- (NSString*) emailAtIndex:(NSInteger)idx;
@end
