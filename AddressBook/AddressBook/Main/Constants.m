//
// Created by Marko Cicak on 4/7/15.
// Copyright (c) 2015 codecentric d.o.o. All rights reserved.
//

NSString* const kCCBaseUrl = @"http://localhost:4567/api";

// Cell Identifiers
NSString* const CellIdAddress = @"AddressCell";
NSString* const CellIdPhone = @"PhoneCell";
NSString* const CellIdEmail = @"EmailCell";
